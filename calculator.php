
<?php
define('DEBUG', false);

if (DEBUG) {
	ini_set('display_startup_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(E_ALL | E_STRICT);
}

function writeLine($str)
{
	echo $str . '<br>';
}

function calculateR($x2, $a, $w, $l)
{
	$X = $x2;
	$y = $a * $X + $w;
	$r = sqrt($X * $X + ($y - $w) * ($y - $w));
	if (DEBUG) {
		writeLine('oto y' . $l . ' koncowe:' . $y);;
		writeLine('oto r.' . $l . ' koncowe:' . $r);
	}
	return $r;
}

$h = null;
if (isset($_POST['h'])) {
	$h = $_POST['h'];
}
?>
<!doctype html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Kalkulator Świata</title>
	<style>
		body {
			background-color: #2D2823;
			color: #EFE9D9;
			font-size: large;
		}
		h1 {
			color: #92C08C;
			margin-left: 40px;
		}
		a:visited {
			color: #dddddd;
		}
		a {
			text-decoration: solid;
			color: #dddddd;
		}
		#container {
			background-color: #61564B;
			margin: 3em auto;
			height: 20em;
			width: 60%;
			text-align: center;
			border: double black 4px;
		}
		#wynik {
			font-size: 150%;
		}
		.inst{
			font-size: 120%;
		}
		@media screen and (max-width: 700px) {
			#container {
				margin: 1% auto;
				height: 100%;
				width: 100%;
			}
			body{
				font-size: inherit;
			}
		}
	</style>
</head>

<body>
<div id="container">
	<h1>Kalkulator świata</h1>
<form action="" method="POST">
	<label for="h" class="inst">Wprowadź H:</label>
	<input type="text" id="h" name="h" value="<?php if ($h != null) echo $h; else echo '0'; ?>">
	<input type="submit" value="OK">
</form>
	<br/>
	<br/>
	<?php
	$r = 6367000;

	if ($h != null) {
		if(is_numeric($h)) {
			$w = $r + $h;
			$a = -sqrt((($w * $w) / ($r * $r)) - 1);
			$d = (4 * ($r * $r * ($a * $a + 1) - $w * $w));
			$delta = sqrt($d);

			if ($d < 0) writeLine('<b>∆<0</b><br><b> DELTA UJEMNA!</b><br>');

			if (DEBUG) {
				writeLine('delta ∆=' . $d);
				writeLine('pierwiastek √∆=' . $delta);
			}

			$x1 = (-2 * $a * $w - $delta) / 2 * ($a * $a + 1);
			$x2 = (-2 * $a * $w + $delta) / 2 * ($a * $a + 1);

			if (DEBUG) {
				writeLine('x1=' . $x1);
				writeLine('x2=' . $x2);
			}

			$r1 = calculateR($x1, $a, $w, '1');
			$r2 = calculateR($x2, $a, $w, '2');
			$wynik = 0;

			if ($delta == 0) {
				$wynik = $r1;
			} else {
				$wynik = ($r1 + $r2) / 2;
			}
			writeLine('<b id="wynik">Wynik:</b>');
			writeLine('<b>' . $wynik . '</b>');
			if (is_nan($wynik)) writeLine('( Not A Number, rownanie nieoznaczone)');
		}else{
			writeLine('Podaj poprawne H');
		}
	}
	writeLine('');
	writeLine('Copyright 2014 © <a href="https://bitbucket.org/Toumash">Tomasz Dłuski</a> & <a href="https://www.facebook.com/olek.nowak.583">Aleksander Nowak</a>');
	?>
</div>

</body>
</html>
